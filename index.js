const express=require("express");
require("dotenv").config();
var config = require("./dbconfig.js");

const mariadb = require("mariadb");
const { json } = require("express");
const pool = mariadb.createPool(config);

const app = express();
app.use(express.json());

app.get("/data", (req, res) => {
    const data = ['data2', 'data3', 'data4', 'data5'];
    res.status(200).json({ data });
});

app.listen(5000);

console.log("Server listen 4000");